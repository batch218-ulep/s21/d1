//console.log("Hello World!");

//[SECTION] Arrays
//Array are used to store multiple related values in a single variable
//Ussually we declare an array using square brackets ([]) also knownas "Array Literals"
/*
        - Arrays are used to store multiple related values in a single variable.
        - They are declared using square brackets ([]) also known as "Array Literals"
        - Arrays it also provides access to a number of functions/methods that help in manipulation array.
            - Methods are used to manipulate information stored within the same object.
        - Array are also objects which is another data type.
        - The main difference of arrays with object is that it contains information in a form of "list" unlike objects which uses "properties" (key-value pair).
        - Syntax:
            let/const arrayName = [elementA, elementB, elementC, ... , ElementNth];
    */

let studentNameA = '2020-1923';
let studentNameB = '2020-1924';
let studentNameC = '2020-1925';
let studentNameD = '2020-1926';
let studentNameE = '2020-1927';

console.log(studentNameA);
console.log(studentNameB);
console.log(studentNameC);
console.log(studentNameD);
console.log(studentNameE);

let studentNumbers1 = ["2020-1923",
					 "2020-1924",
					 "2020-1925",
					 "2020-1926",
					 "2020-1927"];
console.log(studentNumbers1);

//Declaration of an Array
//SYNTAX --> let/cons arrayName = [elementA, elementB, elementC....];

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926'];
console.log(studentNumbers);

console.log(" ");

console.log("Example of Arrays: ")
//Common example of arrays
let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];

//Possible use of an array but is not recommended
let mixedArr = [12, "Asus", false, null, undefined, {}];

console.log(grades);
console.log(computerBrands);

console.log(mixedArr);

console.log(" ");

//Alternative way of writing an array

let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake mongoDb"
	];

console.log(myTasks);

//Create an array with values from variable
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];

console.log(cities);

console.log(" ");

//[SECTION] Length Property
//The .length property allows us to get and set the total number of items in an array.

let fullName = "Alden Ulep";
console.log("Length / size of fullName array " +fullName.length);
console.log("Length / size of cities array " +cities.length);

console.log(" ");

//length property can also set the total number of items in an array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length property of an array.

//We use .length-1 to delete the last item in an array
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

//To delete a specific item in an array we can employ array methods (which will be shown in the next session) or an algorithm (set of code to process tasks).

//Another way to delete an item on an array
//Another example using decrementation (--)
cities.length--;
console.log(cities);

//We can't do same on strings, however.
fullName.length = fullName.length-1;
console.log(fullName.length);
fullName.length--;
console.log(fullName);

console.log(' ');

//If you can shorten the array by setting the length property, you can also lengthen it by adding a number into the length property. Since we lengthen the array forcibly, there will be another item in the array, however, it will be empty or undefined. Like adding another seat but having no one to sit on it.

//Adding an item in an array using incrementation (++)
let theBeatles = ["John", "Paul", "Ringo", "George"];

theBeatles[4] = "Cardo";
theBeatles[theBeatles.length] = "Alden";

console.log(theBeatles);

console.log(' ');

//[SECTION] Reading from Arrays
//Accesing array elements is one of the more common tasks that we do with an array.
//We can access a specific data in an array using an index
//Each element in an array is associated with it's own index/number
    /*
        - Accessing array elements is one of the common task that we do with an array.
        - This can be done through the use of array indexes.
        - Each element in an array is associated with it's own index number.
        - The first element in an array is associated with the number 0, and increasing this number by 1 for every element.
        - Array indexes it is actually refer to a memory address/location

        Array Address: 0x7ffe942bad0
        Array[0] = 0x7ffe942bad0
        Array[1] = 0x7ffe942bad4
        Array[2] = 0x7ffe942bad8

        - Syntax
            arrayName[index];
    */

console.log(grades[0]); //98.5
console.log(computerBrands[3]); //Neo

console.log(grades[20]); //undefined

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem", "Westbrook"];
console.log(lakersLegends[1]);
console.log(lakersLegends[3]);
console.log(lakersLegends[0]);

console.log(" ");

//You can also save/store array items in another variable:

let currentLaker = lakersLegends[2];
console.log(currentLaker);

//You can also reassign array values using the items' indices
console.log("Array before re-assignement");
console.log(lakersLegends);
lakersLegends[2] = "Gasol";
console.log("Array after re-assignment");
console.log(lakersLegends);

console.log(' ');

//Accessing the last element of an array
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegends.length-1;

console.log(bullsLegends[lastElementIndex]);

//You can also add it directly
console.log(bullsLegends[bullsLegends.length-1]);

console.log(' ');

//Adding items to an array
//Using indeces, you can also add iems into the array

let newArr = [];
console.log(newArr[0]); //undefined

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]); //undefined
newArr[1] = "Tifa Lockhart";
console.log(newArr);

console.log(" ");
//You can also add items at the end of the array. Instead of adding it in the front to avoid the risk of replacing the first items in the array:
//newArr[newArr.length-1] = "Aerith Gainsborough"; //- will overwrite the last item instead.

newArr[newArr.length] = "Barett Wallace";
console.log(newArr);

console.log(' ');

//Looping over an Array
//You can use a for loop to iterate over all items in an array
//Set the counter as the index and set the condition that as long as the current index iterated is less than the lenght of the array.
//  declaration/initialization;	condition;	change of value		
for(let index = 0; index < newArr.length; index++){
	//index = 0 < 3 --> T -> "Cloud Strife"
	//index = 1 < 3 --> T -> "Tifa Lockhart"
	//index = 2 < 3 --> T -> "Barett Wallace"
	//index = 3 < 3 --> F -> break/stop for loops
	//You can use the loop counter as index to be able to show each array items in the console.
	console.log(newArr[index]);
}

console.log(' ');

let numArr = [5, 12, 30, 46, 40, 52];

for(let index = 0; index < numArr.length; index++){

	if(numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5");
	}else{
		console.log(numArr[index] + " is not divisible by 5");
	}
}

console.log(' ');
//[SECTION] Multdimensional Array

/*
    - Multidimensional arrays are useful for storing complex data structures
    - A practical application of this is to help visualize/create real world objects
    - Though useful in a number of cases, creating complex array structures is not always recommended.
*/

//8x8 dimension
let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard);

//[1] --> column
//[4] --> row
console.log(chessBoard[1][4]);

console.log(chessBoard[4][3]);
console.table(chessBoard);

console.log("Pawn moves to: " + chessBoard[1][5]);

